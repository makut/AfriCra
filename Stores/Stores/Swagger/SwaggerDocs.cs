﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stores.Swagger
{
    public static class SwaggerDocs
    {
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Creative Solutiion Stores API Documentation",
                    Version = "v1",
                    Description = "Store API Documentation",
                    TermsOfService = "https://hubtel.com/legal/terms/",
                    Contact = new Contact
                    {
                        Name = "Dorcas Maku Tamatey",
                        Email = "dorcas@hubtel.com",
                        Url = "https://developers.hubtel.com"
                    },
                    License = new License
                    {
                        Name = "None",
                        Url = "None"
                    }
                });
                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                {

                };


                //var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                //    "Hubtel.GeneralServices.xml");
                //c.IncludeXmlComments(filePath);
            });

            return services;
        }
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Creative Solution Stores API Documentation");
                c.DocumentTitle = "Creative Stores";
                c.DocExpansion(DocExpansion.None);
            });

            return app;
        }
    }
}
