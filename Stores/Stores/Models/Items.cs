﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stores.Models
{
    public class Items
    {
        public int ItemsId { get; set; }
        public string ItemName { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string FIle { get; set; }
        public string UsersId { get; set; }
    }
}
