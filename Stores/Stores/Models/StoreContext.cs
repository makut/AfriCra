﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Stores.Models;

namespace Stores.Models
{
    public class StoreContext:DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
            
        }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<Items> Items { get; set; }
        public DbSet<Customers> Orders { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Stores.Models.Orders> Orders_1 { get; set; }
    }
}
