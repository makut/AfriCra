﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stores.Models
{
    public class Users
    {
        public int UsersId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string BusinessAddress { get; set; }
        public string password { get; set; }

    }
}
