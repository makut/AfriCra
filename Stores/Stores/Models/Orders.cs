﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stores.Models
{
    public class Orders
    {
        public string OrdersId { get; set; }
        public string ItemsId { get; set; }
        public string CustomersId { get; set; }
        public string UsersId { get; set; }
    }
}
